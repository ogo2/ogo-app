import axios from 'axios';
import { AllProductResponse } from './productResponseType';

function getAllProductsApi() {
  return axios.get<AllProductResponse>('products');
}

const productApi = {
  getAllProducts: getAllProductsApi,
};

export default productApi;
