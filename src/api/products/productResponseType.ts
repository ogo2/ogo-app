import { Product } from 'models/Product';

export type AllProductResponse = {
  data: Array<Product>;
  status: number;
};
