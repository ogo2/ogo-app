import HeaderComponent from 'components/header';
import useProducts from 'hooks/product/useProducts';
import React from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import globalStyle from 'themes/GlobalStyle';
import styles from './styles';

function TestScreen() {
  useProducts();
  return (
    <View style={styles.max}>
      <HeaderComponent
        renderMiddle={() => <Text style={globalStyle.headerText}>Middle</Text>}
        renderLeft={() => (
          <TouchableWithoutFeedback>
            <Text>left</Text>
          </TouchableWithoutFeedback>
        )}
        renderRight={() => (
          <TouchableWithoutFeedback>
            <Text>Right</Text>
          </TouchableWithoutFeedback>
        )}
      />
      <Text>sad</Text>
    </View>
  );
}

export default React.memo(TestScreen);
