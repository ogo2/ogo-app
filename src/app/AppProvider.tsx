/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import { NavigationContainer } from '@react-navigation/native';
import SetupAPI from 'api/config';
import MainStack from 'navigation/MainStack';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import store, { persistor } from 'redux/store';

class AppProvider extends React.PureComponent {
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount() {
    SetupAPI.init();
    SetupAPI.setBaseUrl();
  }

  render() {
    return (
      <SafeAreaProvider>
        <NavigationContainer>
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <MainStack />
            </PersistGate>
          </Provider>
        </NavigationContainer>
      </SafeAreaProvider>
    );
  }
}

export default AppProvider;
