// eslint-disable-next-line no-shadow
enum ROUTES {
  TEST_SCREEN = 'TEST_SCREEN',
  LIVE_STREAM_SCREEN = 'LIVE_STREAM_SCREEN',
  BOTTOM_TAB = 'BOTTOM_TAB',
}

export default ROUTES;
