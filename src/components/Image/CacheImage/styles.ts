import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  positionAbsolute: { position: 'absolute' },
});
