import React, { memo, useCallback, useMemo, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';
import FastImage, { ImageStyle } from 'react-native-fast-image';
import Colors from 'themes/CommonColors';
import styles from './styles';


declare type ResizeMode = 'contain' | 'cover' | 'stretch' | 'center';

type FastImageProp = {
  imageStyle: ImageStyle;
  uri?: string | null | undefined;
  resizeMode?: ResizeMode;
  loadingImage?: boolean;
  source?: any;
};

const CacheImage = ({ imageStyle, uri, resizeMode = 'cover', loadingImage, source }: FastImageProp) => {
  const [isLoadingImage, setIsLoadingImage] = useState(false);

  const newLocal = '../../../../assets/images/bitcoin.jpg';
  const sourceImage = useMemo(
    () => (uri ? { uri } : source ?? require(newLocal)), // test require default
    [uri, source]
  );

  const setImageLoading = useCallback(() => {
    setIsLoadingImage(!isLoadingImage);
  }, [setIsLoadingImage, isLoadingImage]);

  const onLoadImage = useCallback(
    () => (loadingImage ? setImageLoading() : null),
    [loadingImage, setImageLoading]
  );

  return (
    <View>
    <FastImage
        resizeMode= { resizeMode }
  style = { imageStyle }
  source = { sourceImage }
  onLoadStart = { onLoadImage }
  onLoadEnd = { onLoadImage }
    />
    {(isLoadingImage || loadingImage) && (
      <ActivityIndicator style={ styles.positionAbsolute } color = { Colors.black } size = "large" />
      )}
</View>
  );
};

export default memo(CacheImage);
