import React, { ReactNode } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Colors from 'themes/CommonColors';
import CommonHeights from 'themes/CommonHeights';
import CommonWidths from 'themes/CommonWidths';

type CallbackFunctionVariadicAnyReturn = () => ReactNode;
type Props = {
  renderLeft?: CallbackFunctionVariadicAnyReturn;
  renderRight?: CallbackFunctionVariadicAnyReturn;
  renderMiddle?: string | CallbackFunctionVariadicAnyReturn;
  noDivider?: boolean;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderBottomColor: Colors.bottomTabInactiveGray,
    height: Platform.select({
      android: 56,
      default: 44,
    }),
    marginBottom: CommonHeights.secondarySpacing,
    paddingHorizontal: CommonWidths.horizontalSpacing,
  },
  containerLeft: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  containerMiddle: {
    flex: 6,
    justifyContent: 'center',
  },
  containerRight: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});

function renderTitle(renderMiddle: Props['renderMiddle']) {
  if (!renderMiddle) {
    return null;
  } else if (typeof renderMiddle === 'string') {
    return <Text>{renderMiddle}</Text>;
  } else {
    return renderMiddle();
  }
}

export default React.memo((props: Props) => {
  const insets = useSafeAreaInsets();

  return (
    <View style={[styles.container, { marginTop: insets.top }]}>
      {<View style={styles.containerLeft}>{props.renderLeft && props.renderLeft()}</View>}
      {<View style={styles.containerMiddle}>{renderTitle(props.renderMiddle)}</View>}
      {<View style={styles.containerRight}>{props.renderRight && props.renderRight()}</View>}
    </View>
  );
});
