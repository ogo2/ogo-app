import { createStackNavigator } from '@react-navigation/stack';
import React, { Component } from 'react';
import ROUTES from 'constants/ConstRoute';
import TestScreen from 'screens/TestScreen';
import BottomTabStack from './BottomTabStack';
import { MainStackParamsList } from './NavigationsParamsTypes';
import LiveStreamScreen from 'screens/LiveStreamScreen';

const Stack = createStackNavigator<MainStackParamsList>();

export default class MainStack extends Component {
  render() {
    return (
      <Stack.Navigator initialRouteName={ROUTES.TEST_SCREEN} headerMode="none" mode="card">
        <Stack.Screen name={ROUTES.TEST_SCREEN} component={TestScreen} />
        <Stack.Screen name={ROUTES.BOTTOM_TAB} component={BottomTabStack} />
        <Stack.Screen name={ROUTES.LIVE_STREAM_SCREEN} component={LiveStreamScreen} />
      </Stack.Navigator>
    );
  }
}
