import ROUTES from 'constants/ConstRoute';

export type MainStackParamsList = {
  [ROUTES.TEST_SCREEN]: undefined;
  [ROUTES.BOTTOM_TAB]: undefined;
  [ROUTES.LIVE_STREAM_SCREEN]: undefined;
};
