import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ROUTES from 'constants/ConstRoute';
import React from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import LiveStreamScreen from 'screens/LiveStreamScreen';
import TestScreen from 'screens/TestScreen';

const Tab = createBottomTabNavigator();

const BottomTabStack = () => {
  const insets = useSafeAreaInsets();

  return (
    <Tab.Navigator
      tabBarOptions={{
        safeAreaInsets: { bottom: -insets.bottom },
      }}
    >
      <Tab.Screen name={ROUTES.TEST_SCREEN} component={TestScreen} />
      <Tab.Screen name={ROUTES.LIVE_STREAM_SCREEN} component={LiveStreamScreen} />
    </Tab.Navigator>
  );
};

export default BottomTabStack;
