import productApi from 'api/products/productApi';
import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { getAllProductsSuccess } from 'redux/slices/product';

export default function useProducts() {
  const dispatch = useDispatch();

  const getListProducts = useCallback(async () => {
    const response = await productApi.getAllProducts();
    if (response.status === 200) {
      console.log('response', response.data);
      dispatch(
        getAllProductsSuccess({
          products: response?.data,
        })
      );
    }
  }, []);

  useEffect(() => {
    getListProducts();
  }, []);

  return useMemo(
    () => ({
      getListProducts,
    }),
    []
  );
}
