const Colors = {
  // common colors
  black: '#000000',
  white: '#ffffff',
  red: '#ff0000',
  blue: '#001cff',
  transparent: 'transparent',

  // project colors
  bottomTabInactiveGray: 'rgba(213,216,225,1)',
  backgroundWhite: 'rgba(254,254,255,1)',
  categoryGray: 'rgba(224,223,226,1)',
  inActiveCategoryGray: 'rgba(192,192,194,1)',
  bottomActiveBlue: 'rgba(52,74,1199,1)',
  mainBackgroundColorContainer: "red"
};

export default Colors;
