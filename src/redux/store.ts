import AsyncStorage from '@react-native-async-storage/async-storage';
import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { Middleware, Reducer } from "redux";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  Persistor,
  persistReducer,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE
} from 'redux-persist';
import { PersistConfig } from 'redux-persist/es/types';
import productReducer from 'redux/slices/product';

const persistConfig: PersistConfig<RootState> = {
  key: 'root',
  version: 1,
  storage: AsyncStorage,
};

const rootReducer: Reducer = combineReducers({
  product: productReducer,
});

const persistedReducer: Reducer = persistReducer(persistConfig, rootReducer);

const finalMiddleware: Array<Middleware> = [
  ...getDefaultMiddleware({
    thunk: true,
    immutableCheck: true,
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
];

const store = configureStore({
  reducer: persistedReducer,
  middleware: finalMiddleware,
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>
export const persistor: Persistor = persistStore(store);
export default store;
