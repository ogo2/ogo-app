import { Product } from "models/Product";

export type InitialProductState = {
  listProducts: Array<Product>
}