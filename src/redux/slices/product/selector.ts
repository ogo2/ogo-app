import { RootState } from "redux/store";

const getAllProducts = (state: RootState) => state?.products?.listProducts || {};

const SessionSelector = {
  getAllProducts,
};

export default SessionSelector;
