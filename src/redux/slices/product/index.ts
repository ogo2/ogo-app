import { createSlice, Slice } from '@reduxjs/toolkit';
import { InitialProductState } from './type';

export const initialState: InitialProductState = {
  listProducts: [],
};

export const counterSlice: Slice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    getAllProductsSuccess: (state, action) => {
      const { products } = action.payload;
      state.listProducts = products;
    },
  },
});

export const { getAllProductsSuccess } = counterSlice.actions;

export default counterSlice.reducer;
